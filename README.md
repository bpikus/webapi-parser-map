# `webapi-parser-map`

Package for uploading and managing the parser-map singleton.

### Cutting a release

1. Make necessary edits to the mapping.
2. Bump the semver in the manifest.json

## Develop

### Build & Load

Changes in this repository do not directly affect any REST installations. Once a change is made, the package will need to be loaded into any pertinent installations.

